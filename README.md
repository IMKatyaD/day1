Assessment: Getting Started with GitLab

For this assessment, you will practice using command line instructions to create several files and directories while using git version control to track your changes.

Instructions

1. If you have not already done so for previous Activities, create and setup your GitLab account.
2. If you have not already done so for previous Activities, setup new SSH keys on your computer, and add the public keys to your GitLab account.
3. Create a new directory on your computer for this project called "day1" and initialize it as a git repository.
4. Create a short README.md file. Use git to add and commit this new file.
5. Create two new directories within your project; name them "dir1" and "dir2".
6. Inside dir1, create a file named "file1"; inside dir2 create a file named "file2".
7. Use git to add and commit the new directories to your local repository.
8. Connect your new local repository to a GitLab repository and push it.
9. Submit the GitLab repository URL below. It should look like "https://gitlab.com/your_username/day1"